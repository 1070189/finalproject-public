FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/data

WORKDIR /usr/src/data/

COPY target/smarthome-1.0-SNAPSHOT.jar /usr/src/data/

CMD /usr/src/data/java -jar smarthome-1.0-SNAPSHOT.jar
