package pt.ipp.isep.dei.project1.model.geographicarea;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pt.ipp.isep.dei.project1.model.repositories.GeographicAreaTypeRepository;
import pt.ipp.isep.dei.project1.model.repositories.GeographicAreaTypeRepo;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GeographicAreasTypesTestImpl {

    @Mock
    private GeographicAreaTypeRepository geographicAreaTypeRepository;
    @Mock
    private GeographicAreaTypeRepo geographicAreaTypeRepo;

    @BeforeEach
    void initUseCase() {
        geographicAreaTypeRepo = new GeographicAreaTypeRepo(geographicAreaTypeRepository);
    }

    @Test
    void testGetType() {
        //Arrange
        GeographicAreaType geo1 = new GeographicAreaType("rua");
        GeographicAreaType geo2 = new GeographicAreaType("avenida");
        List<GeographicAreaType> expectedResult = new ArrayList<>();
        expectedResult.add(geo1);
        expectedResult.add(geo2);
        //Act
        geographicAreaTypeRepo.add(geo1);
        geographicAreaTypeRepo.add(geo2);
        List<GeographicAreaType> result = geographicAreaTypeRepo.getListOfGeographicAreaType();
        //Assert
        assertEquals(expectedResult, result);
    }


    @Test
    void testAddType() {
        //Arrange
        GeographicAreaType geo1 = new GeographicAreaType("rua");
        GeographicAreaType geo2 = new GeographicAreaType("avenida");
        List<GeographicAreaType> expectedResult = new ArrayList<>();
        expectedResult.add(geo1);
        expectedResult.add(geo2);
        //Act
        geographicAreaTypeRepo.add(geo1);
        boolean result = geographicAreaTypeRepo.add(geo2);
        //Assert
        assertTrue(result);
    }

    @Test
    void testAddTypeRepeated() {
        //Arrange
        GeographicAreaType geo1 = new GeographicAreaType("rua");
        GeographicAreaType geo2 = new GeographicAreaType("rua");
        List<GeographicAreaType> expectedResult = new ArrayList<>();
        expectedResult.add(geo1);
        expectedResult.add(geo2);
        //Act
        geographicAreaTypeRepo.add(geo1);
        boolean result = geographicAreaTypeRepo.add(geo2);
        //Assert
        assertFalse(result);
    }

    @Test
    void testGetTypeByName() {
        //Arrange
        GeographicAreaType geo1 = new GeographicAreaType("rua");
        GeographicAreaType expectedResult = geo1;
        geo1.setType("rua");
        //Act
        geographicAreaTypeRepo.add(geo1);
        GeographicAreaType result = geographicAreaTypeRepo.getTypeByName("rua");
        //Assert
        assertEquals(expectedResult,result);
    }


    @Test
    void testGetTypeByIdNull() {
        //Arrange
        GeographicAreaType geo1 = new GeographicAreaType("rua");
        GeographicAreaType expectedResult = null;
        geo1.setType("rua");
        //Act
        geographicAreaTypeRepo.add(geo1);
        GeographicAreaType result = geographicAreaTypeRepo.getTypeByName("cidade");
        //Assert
        assertEquals(expectedResult,result);
    }

}