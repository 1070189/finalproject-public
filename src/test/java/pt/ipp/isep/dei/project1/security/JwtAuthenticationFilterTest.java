package pt.ipp.isep.dei.project1.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletContext;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.cors.CorsUtils;


import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

@ExtendWith(MockitoExtension.class)
public class JwtAuthenticationFilterTest {

    @Mock
    AuthenticationManager authenticationManager;


    MockHttpServletRequestBuilder builder;

    JwtAuthenticationFilter jwt;

    @BeforeEach
    void initUseCase() {
        jwt = new JwtAuthenticationFilter(authenticationManager);
    }

    /*
    @Test
    public void attemptAuthentication() throws IOException {

        builder.content("user");


        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addParameter("username","user");
        request.addParameter("password","user123");


        builder.postProcessRequest(request);

        System.out.println(request.getParameter("name"));




        LoginViewModel credentials  = new ObjectMapper().readValue(request.getInputStream(), LoginViewModel.class);

        System.out.println(credentials.getUsername());
    }*/

    @Test
    public void attemptAuthenticationv2() throws IOException, JSONException {


        JSONObject jsonObject = new JSONObject("{\"username\":\"user\",\"password\":\"user123\"}");

        String credentials =jsonObject.toString();

     //   System.out.println(credentials);

        byte[] contentBody = credentials.getBytes();

      //  System.out.println(contentBody.toString());

        MockServletContext servletContext = new MockServletContext();
        MockHttpServletRequest request = new MockHttpServletRequest(servletContext);
        MockHttpServletResponse response = new MockHttpServletResponse();

        request.setContent(contentBody);

        request.setRemoteAddr("http://localhost:3000");
        request.setSecure(true);
        request.setLocalAddr("http://localhost:3000");




     //   System.out.println("IS CORS PREFLIGHT? - "+CorsUtils.isPreFlightRequest(request));

        response.setStatus(HttpServletResponse.SC_OK);

        LoginViewModel result = new ObjectMapper().readValue(request.getInputStream(), LoginViewModel.class);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                result.getUsername(),
                result.getPassword(),
                new ArrayList<>());

        Authentication auth = authenticationManager.authenticate(authenticationToken);

       // System.out.println(auth);

        assertFalse(CorsUtils.isPreFlightRequest(request));
    }

    /*
    @Test
    public void successfulAuthentication() {
    }*/
}