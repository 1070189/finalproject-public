package pt.ipp.isep.dei.project1.model.geographicarea;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pt.ipp.isep.dei.project1.model.repositories.GeographicAreaTypeRepository;
import pt.ipp.isep.dei.project1.model.sensor.SensorType;
import pt.ipp.isep.dei.project1.model.repositories.GeographicAreaTypeRepo;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GeographicAreaTypeTest {

    @Mock
    private GeographicAreaTypeRepository geographicAreaTypeRepository;
    @Mock
    private GeographicAreaTypeRepo geographicAreaTypeRepo;

    @BeforeEach
    void initUseCase() {
        geographicAreaTypeRepo = new GeographicAreaTypeRepo(geographicAreaTypeRepository);
    }

    @Test
    public void testGetName() {
        //ARRANGE
        GeographicAreaType s1 = new GeographicAreaType();
        s1.setType("GeographicAreaType1");
        String expectedResult = "GeographicAreaType1";
        //ACT
        String result = s1.getType();
        //ASSERT
        assertEquals(expectedResult, result);
    }

    @Test
    public void equalsTrueTest() {
        //ARRANGE
        GeographicAreaType s1 = new GeographicAreaType("GeographicAreaType1");
        Object obj = new GeographicAreaType("GeographicAreaType1");
        //ACT
        boolean result = s1.equals(obj);
        //ASSERT
        assertTrue(result);
    }

    @Test
    public void equalsTrueTestMyself() {
        //ARRANGE
        GeographicAreaType s1 = new GeographicAreaType("GeographicAreaType1");
        Object obj = s1;
        boolean result = s1.equals(obj);
        //ASSERT
        assertTrue(result);
    }

    @Test
    public void equalsFalseTest() {
        //ARRANGE
        GeographicAreaType s1 = new GeographicAreaType("GeographicAreaType1");
        Object obj = new GeographicAreaType("GeographicAreaType2");
        //ACT
        boolean result = s1.equals(obj);
        //ASSERT
        assertFalse(result);
    }

    @Test
    public void equalsFalseTestOtherClass() {
        //ARRANGE
        GeographicAreaType s1 = new GeographicAreaType("GeographicAreaType1");
        Object obj = new SensorType("temperature");
        //ACT
        boolean result = s1.equals(obj);
        //ASSERT
        assertFalse(result);
    }

    @Test
    public void testHashCode() {
        //ARRANGE
        GeographicAreaType s1 = new GeographicAreaType("GeographicAreaType1");
        //ACT
        int result = s1.getType().charAt(0);
        //ASSERT
        assertEquals(s1.hashCode(), result);
    }



    @Test
    public void createGeographicAreaTypeValidNameEmpty() {
        try {
            GeographicAreaType GAT1 = new GeographicAreaType("");
            fail("Fail. An exception must be thrown");
        } catch (RuntimeException e) {
            assertEquals("Insert a valid name of Geographic Area Type", e.getMessage());
        }
    }

    @Test
    public void createGeographicAreaTypeValidNameNull() {
        try {
            GeographicAreaType GAT1 = new GeographicAreaType(null);
            fail("Fail. An exception must be thrown");
        } catch (RuntimeException e) {
            assertEquals("Insert a valid name of Geographic Area Type", e.getMessage());
        }
    }


    @Test
    public void testToString() {
        //ARRANGE
        GeographicAreaType s1 = new GeographicAreaType("GeographicAreaType1");
        String expectedResult = "GeographicAreaType1" + "\n";
        //ACT
        String result = s1.toString();
        //ASSERT
        assertEquals(expectedResult, result);
    }

    @Test
    void getGeographicAreaTypeByID() {
        GeographicAreaType s1 = new GeographicAreaType("GeographicAreaType1");
        s1.setType("GeographicAreaType1");
        assertEquals("GeographicAreaType1", s1.getType());
    }


}

