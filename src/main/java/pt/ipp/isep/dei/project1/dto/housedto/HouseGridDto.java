package pt.ipp.isep.dei.project1.dto.housedto;


import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.ResourceSupport;
import java.util.List;

public class HouseGridDto extends ResourceSupport {

    private String mCode;
    @Getter @Setter
    private List<String> roomList;

    public String getCode() {
        return mCode;
    }

    public void setCode(String mCode) {
        this.mCode = mCode;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof HouseGridDto))
            return false;
        HouseGridDto c = (HouseGridDto) obj;
        return this.mCode.equals(c.mCode);
    }

    @Override
    public int hashCode() {
        return this.mCode.charAt(0);
    }

}
