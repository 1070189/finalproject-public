package pt.ipp.isep.dei.project1.controllersweb.room;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pt.ipp.isep.dei.project1.controllers.house.ImportRoomSensorsFromFileController;
import pt.ipp.isep.dei.project1.model.house.Room;
import pt.ipp.isep.dei.project1.model.repositories.RoomDomainService;
import pt.ipp.isep.dei.project1.readers.readerjson.RoomSensorsFromJson;

import java.io.File;
import java.io.IOException;
import java.util.List;


@RestController
@RequestMapping(value = "/room-configuration")
@CrossOrigin(origins={"http://localhost:3000","http://192.168.33.13:3000"}, maxAge = 3600)
public class ImportRoomSensorsWebController {

    private final RoomDomainService roomDomainService;
    private final ImportRoomSensorsFromFileController controller;

    @Autowired
    public ImportRoomSensorsWebController(RoomDomainService roomDomainService, ImportRoomSensorsFromFileController controller) {
        this.roomDomainService = roomDomainService;
        this.controller = controller;
    }

    @PostMapping(value = "/import-sensors")
    public ResponseEntity<Object> importRoomSensors(@RequestBody String path) throws IOException {
        /**List<String[]> list = new ArrayList<>();**/
        List<Room> listOfRooms;
        ObjectMapper objectMapper = new ObjectMapper();
        File file = new File(path);
        RoomSensorsFromJson roomSensorsFromJson = objectMapper.readValue(file, RoomSensorsFromJson.class);
        /**  try {*/
        listOfRooms = roomDomainService.getListOfRooms();
        /**  } catch (NullPointerException e) {
         String[] string = new String[2];                         Desde que se comecou a usar roomService já não se apanha esta exception
         string[0] = "The House doesn't have rooms yet. ";
         string[1] = "Please add rooms first!";
         list.add(string);
         return list;
         }*/
        return new ResponseEntity<>(controller.setSensorsToRooms(listOfRooms, roomSensorsFromJson), HttpStatus.OK);
    }
}

